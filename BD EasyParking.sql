CREATE DATABASE  IF NOT EXISTS `misiontic` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `misiontic`;
-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: localhost    Database: misiontic
-- ------------------------------------------------------
-- Server version	8.0.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cliente` (
  `idcliente` int NOT NULL AUTO_INCREMENT,
  `idplazadisponible` int DEFAULT NULL,
  `nombre` varchar(45) COLLATE utf8mb4_general_ci NOT NULL,
  `telefono` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `correo` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`idcliente`),
  KEY `cliente_ibfk_1` (`idplazadisponible`),
  CONSTRAINT `cliente_ibfk_1` FOREIGN KEY (`idplazadisponible`) REFERENCES `plazadisponible` (`idplazadisponible`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` VALUES (1,2,'Camilo Hernandez','3205469000','camiloh@gmail.com'),(2,4,'Marleny Agudelo','3124785600','marleny@gmail.com'),(4,1,'Tomas Berrio','3204789554','tomas@gmail.com');
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturacion`
--

DROP TABLE IF EXISTS `facturacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `facturacion` (
  `idfacturacion` int NOT NULL AUTO_INCREMENT,
  `idvehiculo` int DEFAULT NULL,
  `idcliente` int DEFAULT NULL,
  `idplazadisponible` int DEFAULT NULL,
  `idusuario` int DEFAULT NULL,
  `entrada` datetime NOT NULL,
  `salida` datetime NOT NULL,
  `valorapagar` double NOT NULL,
  PRIMARY KEY (`idfacturacion`),
  KEY `facturacion_ibfk_1` (`idusuario`),
  KEY `facturacion_ibfk_2` (`idvehiculo`),
  KEY `facturacion_ibfk_3` (`idcliente`),
  KEY `facturacion_ibfk_4` (`idplazadisponible`),
  CONSTRAINT `facturacion_ibfk_1` FOREIGN KEY (`idusuario`) REFERENCES `usuarios` (`idusuario`),
  CONSTRAINT `facturacion_ibfk_2` FOREIGN KEY (`idvehiculo`) REFERENCES `vehiculo` (`idvehiculo`),
  CONSTRAINT `facturacion_ibfk_3` FOREIGN KEY (`idcliente`) REFERENCES `cliente` (`idcliente`),
  CONSTRAINT `facturacion_ibfk_4` FOREIGN KEY (`idplazadisponible`) REFERENCES `plazadisponible` (`idplazadisponible`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facturacion`
--

LOCK TABLES `facturacion` WRITE;
/*!40000 ALTER TABLE `facturacion` DISABLE KEYS */;
INSERT INTO `facturacion` VALUES (1,1,1,2,1,'2022-02-12 00:53:02','2022-02-12 04:23:12',28000),(2,2,2,4,2,'2022-08-12 22:00:02','2022-02-12 23:53:02',5690),(4,4,4,1,4,'2022-01-12 02:15:02','2022-02-12 06:40:02',99900);
/*!40000 ALTER TABLE `facturacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plazadisponible`
--

DROP TABLE IF EXISTS `plazadisponible`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `plazadisponible` (
  `idplazadisponible` int NOT NULL AUTO_INCREMENT,
  `zonavehiculos` varchar(10) COLLATE utf8mb4_general_ci NOT NULL,
  `limitecarros` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `limitemotos` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `tarifas` varchar(30) COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`idplazadisponible`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plazadisponible`
--

LOCK TABLES `plazadisponible` WRITE;
/*!40000 ALTER TABLE `plazadisponible` DISABLE KEYS */;
INSERT INTO `plazadisponible` VALUES (1,'Motos','','M07','16500.0'),(2,'Motos','','M20','8000.0'),(4,'Motos','','M08','1800.0'),(6,'Carros','C01','','4000.0');
/*!40000 ALTER TABLE `plazadisponible` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reporte`
--

DROP TABLE IF EXISTS `reporte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reporte` (
  `idreporte` int NOT NULL AUTO_INCREMENT,
  `idcliente` int DEFAULT NULL,
  `idfacturacion` int DEFAULT NULL,
  `tiempoconcepto` varchar(45) COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`idreporte`),
  KEY `reporte_ibfk_1` (`idfacturacion`),
  KEY `reporte_ibfk_2` (`idcliente`),
  CONSTRAINT `reporte_ibfk_1` FOREIGN KEY (`idfacturacion`) REFERENCES `facturacion` (`idfacturacion`),
  CONSTRAINT `reporte_ibfk_2` FOREIGN KEY (`idcliente`) REFERENCES `cliente` (`idcliente`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reporte`
--

LOCK TABLES `reporte` WRITE;
/*!40000 ALTER TABLE `reporte` DISABLE KEYS */;
INSERT INTO `reporte` VALUES (1,1,1,'11 Horas + 25 Minutos'),(2,2,2,'1 Hora + 53 Minutos'),(4,4,4,'Mensualidad + 4 Horas + 25 Minutos');
/*!40000 ALTER TABLE `reporte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios` (
  `idusuario` int NOT NULL AUTO_INCREMENT,
  `usuario` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `clave` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `nombre` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `cedula` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `telefono` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `correo` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `activo` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`idusuario`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'Carlos','00000','Carlos Agudelo','1000243231','3005280600','carlos@gmail.com',1),(2,'Juan','frontend','Juan Restrepo','1021458889','3215280612','juan3@gmail.com',0),(4,'Luisa','78965','Luisa Fernanda','1017845960','3015876412','luisa@gmail.com',1),(9,'Marleny','tunnyj','Marleny Orozco','43689410','301888888','marleny2@gmail.com',0);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehiculo`
--

DROP TABLE IF EXISTS `vehiculo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vehiculo` (
  `idvehiculo` int NOT NULL AUTO_INCREMENT,
  `idcliente` int DEFAULT NULL,
  `tipovehiculo` varchar(15) COLLATE utf8mb4_general_ci NOT NULL,
  `matricula` varchar(10) COLLATE utf8mb4_general_ci NOT NULL,
  `descripcion` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`idvehiculo`),
  KEY `vehiculo_ibfk_1` (`idcliente`),
  CONSTRAINT `vehiculo_ibfk_1` FOREIGN KEY (`idcliente`) REFERENCES `cliente` (`idcliente`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehiculo`
--

LOCK TABLES `vehiculo` WRITE;
/*!40000 ALTER TABLE `vehiculo` DISABLE KEYS */;
INSERT INTO `vehiculo` VALUES (1,1,'Moto','CTG333','Yamaha, Verde.'),(2,2,'Moto','TYO456','Honda, amarilla.'),(4,4,'Moto','CTG000','Pulsar, Negra.');
/*!40000 ALTER TABLE `vehiculo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-09-16 23:15:40
