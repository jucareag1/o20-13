CREATE TABLE IF NOT EXISTS facturacion(
id_facturacion INT NOT NULL AUTO_INCREMENT,
id_vehiculo INT NOT NULL,
id_cliente INT NOT NULL,
id_plaza_disponible INT NOT NULL,
id_usuario INT NOT NULL,
fh_entrada DATETIME NOT NULL,
fh_salida DATETIME NOT NULL,
valor_a_pagar DOUBLE NOT NULL,
PRIMARY KEY (id_facturacion),
FOREIGN KEY (id_usuario) REFERENCES usuario (id_usuario),
FOREIGN KEY (id_vehiculo) REFERENCES vehiculo (id_vehiculo),
FOREIGN KEY (id_cliente) REFERENCES cliente (id_cliente),
FOREIGN KEY (id_plaza_disponible) REFERENCES plaza_disponible (id_plaza_disponible))
ENGINE = InnoDB;