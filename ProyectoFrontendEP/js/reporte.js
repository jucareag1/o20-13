//METODO PARA LISTAR
function listarReporte()
{
    let request = sendRequest('reporte/list', 'GET', '');
    let tabla = document.getElementById('tablareporte');
    tabla.innerHTML = "";
    request.onload = function()
    {
        let datos = request.response;
        datos.forEach(element =>
        {
            tabla.innerHTML += `
            <tr>
                <th scope="row">${element.idreporte}</th>
                <td>${element.idcliente}</td>
                <td>${element.idfacturacion}</td>
                <td>${element.tiempoconcepto}</td>                              
                <td>
                    <button type="button" class="btn btn-primary" 
                    onclick='window.location = "form_reporte.html?id=${element.idreporte}"'>Editar</button>
                    <button type="button" class="btn btn-danger" 
                    onclick='borrarReporte(${element.idreporte})'>Eliminar</button>
                </td>
            </tr>
            `
        });
    }
    request.onerror = function()
    {
        tabla.innerHTML = `
        <tr>
            <td colspan='6'>Error al cargar los datos</td>
        </tr>
        `;
    }
}

//METODO PARA BORRAR
function borrarReporte(id)
{
    let request = sendRequest('reporte/'+id, 'DELETE', '');
    request.onload = function()
    {
        listarReporte();
    }
}

//METODO PARA CARGAR
function cargarReporte(id)
{
    let request = sendRequest('reporte/list/'+id, 'GET', '');
    let idreporte = document.getElementById('idreporte');
    let idcliente = document.getElementById('idcliente');
    let idfacturacion = document.getElementById('idfacturacion');
    let tiempoconcepto = document.getElementById('tiempoconcepto');
    request.onload = function()
    {
        let datos = request.response;
        idreporte.value = datos.idreporte;
        idcliente.value = datos.idcliente;
        idfacturacion.value = datos.idfacturacion;
        tiempoconcepto.value = datos.tiempoconcepto;
    }
    request.onerror = function()
    {
        alert("Error al cargar los datos");
    }
}

//METODO PARA AGREGAR O ACTUALIZAR
function grabarReporte()
{
    let idreporte = document.getElementById('idreporte').value;
    let idcliente = document.getElementById('idcliente').value;
    let idfacturacion = document.getElementById('idfacturacion').value;
    let tiempoconcepto = document.getElementById('tiempoconcepto').value;
    let datos = 
    {
        'idreporte':idreporte,
        'idcliente':idcliente,
        'idfacturacion':idfacturacion,
        'tiempoconcepto':tiempoconcepto
    }
    let request = sendRequest('reporte/', idreporte ? 'PUT':'POST', datos)
    request.onload = function()
    {
        window.location = "reporte.html";
    }
    request.onerror = function()
    {
        alert("Error al guardar los cambios.");
    }
}