const url = "http://minticloud.uis.edu.co/c3s20grupo13/"
//const url = "http://localhost:8080/"

//METODO PARA HACER PETICIONES AL SERVIDOR
function sendRequest(endpoint, method, data)
{
    let request = new XMLHttpRequest();
    request.open(method, url+endpoint);
    request.setRequestHeader('Content-Type','application/json');
    //request.setRequestHeader("Access-Control-Allow-Origin", "*");
    //request.setRequestHeader("Access-Control-Allow-Credentials", "true");
    //request.setRequestHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    //request.setRequestHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    request.responseType = 'json';
    request.send(data?JSON.stringify(data): data);
    /*request.onload = function(){
        let data = request.response;
    }
    request.onerror = function(){
        alert("Error.");
    }*/
    return request
}
