//METODO PARA LISTAR
function listarFacturacion()
{
    let request = sendRequest('facturacion/list', 'GET', '');
    let tabla = document.getElementById('tablafacturacion');
    tabla.innerHTML = "";
    request.onload = function()
    {
        let datos = request.response;
        datos.forEach(element =>
        {
            tabla.innerHTML += `
            <tr>
                <th scope="row">${element.idfacturacion}</th>
                <td>${element.entrada}</td>
                <td>${element.salida}</td>
                <td>${element.valorapagar}</td>            
                <td>
                    <button type="button" class="btn btn-primary" 
                    onclick='window.location = "form_facturacion.html?id=${element.idfacturacion}"'>Editar</button>
                    <button type="button" class="btn btn-danger" 
                    onclick='borrarFacturacion(${element.idfacturacion})'>Eliminar</button>
                </td>
            </tr>
            `
        });
    }
    request.onerror = function()
    {
        tabla.innerHTML = `
        <tr>
            <td colspan='6'>Error al cargar los datos</td>
        </tr>
        `;
    }
}

//METODO PARA BORRAR
function borrarFacturacion(id)
{
    let request = sendRequest('facturacion/'+id, 'DELETE', '');
    request.onload = function()
    {
        listarFacturacion();
    }
}

//METODO PARA CARGAR
function cargarFacturacion(id)
{
    let request = sendRequest('facturacion/list/'+id, 'GET', '');
    let idfacturacion = document.getElementById('idfacturacion');
    let idvehiculo = document.getElementById('idvehiculo');
    let idcliente = document.getElementById('idcliente');
    let idplazadisponible = document.getElementById('idplazadisponible');
    let idusuario = document.getElementById('idusuario');
    let entrada = document.getElementById('entrada');
    let salida = document.getElementById('salida');
    let valorapagar = document.getElementById('valorapagar');
    request.onload = function()
    {
        let datos = request.response;
        idfacturacion.value = datos.idfacturacion;
        idvehiculo.value = datos.idvehiculo;
        idcliente.value = datos.idcliente;
        idplazadisponible.value = datos.idplazadisponible;
        idusuario.value = datos.idusuario;
        entrada.value = datos.entrada;
        salida.value = datos.salida;
        valorapagar.value = datos.valorapagar;
    }
    request.onerror = function()
    {
        alert("Error al cargar los datos");
    }
}

//METODO PARA AGREGAR O ACTUALIZAR
function grabarFacturacion()
{
    let idfacturacion = document.getElementById('idfacturacion').value;
    let idvehiculo = document.getElementById('idvehiculo').value;
    let idcliente = document.getElementById('idcliente').value;
    let idplazadisponible = document.getElementById('idplazadisponible').value;
    let idusuario = document.getElementById('idusuario').value;
    let entrada = document.getElementById('entrada').value;
    let salida = document.getElementById('salida').value;
    let valorapagar = document.getElementById('valorapagar').value;
    let datos = {
        'idfacturacion':idfacturacion,
        'idvehiculo': idvehiculo,
        'idcliente' : idcliente,
        'idplazadisponible' : idplazadisponible,
        'idusuario' : idusuario,
        'entrada':entrada, 
        'salida':salida,
        'valorapagar':valorapagar
    }

    let request = sendRequest('facturacion/', idfacturacion ? 'PUT':'POST', datos)
    request.onload = function()
    {
        window.location = "facturacion.html";
    }
    request.onerror = function()
    {
        alert("Error al guardar.");
    }
}