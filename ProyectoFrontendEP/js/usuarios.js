//METODO PARA LISTAR
function listarUsuarios()
{
    let request = sendRequest('usuarios/list', 'GET', '');
    let tabla = document.getElementById('tablausuarios');
    tabla.innerHTML = "";
    request.onload = function()
    {
        let datos = request.response;
        datos.forEach(element =>
        {
            tabla.innerHTML += `
            <tr>
                <th scope="row">${element.idusuario}</th>
                <td>${element.usuario}</td>
                <td>${element.clave}</td>
                <td>${element.nombre}</td>
                <td>${element.cedula}</td>
                <td>${element.telefono}</td>
                <td>${element.correo}</td>
                <td>
                    <div class="form-check form-switch">
                        <input ${element.activo ? "checked" : "unchecked"}
                        class="form-check-input" type="checkbox" role="switch">
                    </div>
                </td>
                <td>
                    <button type="button" class="btn btn-primary" 
                    onclick='window.location = "form_usuarios.html?id=${element.idusuario}"'>Editar</button>
                    <button type="button" class="btn btn-danger" 
                    onclick='borrarUsuario(${element.idusuario})'>Eliminar</button>
                </td>
            </tr>
            `
        });
    }
    request.onerror = function()
    {
        tabla.innerHTML = `
        <tr>
            <td colspan='6'>Error al cargar los datos</td>
        </tr>
        `;
    }
}

//METODO PARA BORRAR
function borrarUsuario(id)
{
    let request = sendRequest('usuarios/'+id, 'DELETE', '');
    request.onload = function()
    {
        listarUsuarios();
    }
}

//METODO PARA CARGAR
function cargarUsuario(id)
{
    let request = sendRequest('usuarios/list/'+id, 'GET', '');
    let idusuario = document.getElementById('idusuario');
    let usuario = document.getElementById('usuario');
    let clave = document.getElementById('clave');
    let nombre = document.getElementById('nombre');
    let cedula = document.getElementById('cedula');
    let telefono = document.getElementById('telefono');
    let correo = document.getElementById('correo');
    let activo = document.getElementById('activo');
    request.onload = function()
    {
        let datos = request.response;
        idusuario.value = datos.idusuario;
        usuario.value = datos.usuario;
        clave.value = datos.clave;
        nombre.value = datos.nombre;
        cedula.value = datos.cedula;
        telefono.value = datos.telefono;
        correo.value = datos.correo;
        activo.checked = datos.activo;
    }
    request.onerror = function()
    {
        alert("Error al cargar los datos");
    }
}

//METODO PARA AGREGAR O ACTUALIZAR
function grabarUsuario()
{
    let idusuario = document.getElementById('idusuario').value;
    let usuario = document.getElementById('usuario').value;
    let clave = document.getElementById('clave').value;
    let nombre = document.getElementById('nombre').value;
    let cedula = document.getElementById('cedula').value;
    let telefono = document.getElementById('telefono').value;
    let correo = document.getElementById('correo').value;
    let activo = document.getElementById('activo').checked;
    let datos = {'idusuario':idusuario,'usuario':usuario,'clave':clave,'nombre':nombre,'cedula':cedula,'telefono':telefono, 'correo':correo,'activo':activo}
    let request = sendRequest('usuarios/', idusuario ? 'PUT':'POST', datos)
    request.onload = function()
    {
        window.location = "usuarios.html";
    }
    request.onerror = function()
    {
        alert("Error al guardar los cambios.");
    }
}