//METODO PARA LISTAR
function listarPlazadisponible()
{
    let request = sendRequest('plazadisponible/list', 'GET', '');
    let tabla = document.getElementById('tablaplazadisponible');
    tabla.innerHTML = "";
    request.onload = function()
    {
        let datos = request.response;
        datos.forEach(element =>
        {
            tabla.innerHTML += `
            <tr>
                <th scope="row">${element.idplazadisponible}</th>
                <td>${element.zonavehiculos}</td>
                <td>${element.limitecarros}</td>
                <td>${element.limitemotos}</td>
                <td>${element.tarifas}</td>
                <td>
                    <button type="button" class="btn btn-primary" 
                    onclick='window.location = "form_plazadisponible.html?id=${element.idplazadisponible}"'>Editar</button>
                    <button type="button" class="btn btn-danger" 
                    onclick='borrarPlazadisponible(${element.idplazadisponible})'>Eliminar</button>
                </td>
            </tr>
            `
        });
    }
    request.onerror = function()
    {
        tabla.innerHTML = `
        <tr>
            <td colspan='6'>Error al cargar los datos</td>
        </tr>
        `;
    }
}

//METODO PARA BORRAR
function borrarPlazadisponible(id)
{
    let request = sendRequest('plazadisponible/'+id, 'DELETE', '');
    request.onload = function()
    {
        listarPlazadisponible();
    }
}

//METODO PARA CARGAR
function cargarPlazadisponible(id)
{
    let request = sendRequest('plazadisponible/list/'+id, 'GET', '');
    let idplazadisponible = document.getElementById('idplazadisponible');
    let zonavehiculos = document.getElementById('zonavehiculos');
    let limitecarros = document.getElementById('limitecarros');
    let limitemotos = document.getElementById('limitemotos');
    let tarifas = document.getElementById('tarifas');
    request.onload = function()
    {
        let datos = request.response;
        idplazadisponible.value = datos.idplazadisponible;
        zonavehiculos.value = datos.zonavehiculos;
        limitecarros.value = datos.limitecarros;
        limitemotos.value = datos.limitemotos;
        tarifas.value = datos.tarifas;       
    }
    request.onerror = function()
    {
        alert("Error al cargar los datos");
    }
}

//METODO PARA AGREGAR O ACTUALIZAR
function grabarPlazadisponible()
{
    let idplazadisponible = document.getElementById('idplazadisponible').value;
    let zonavehiculos = document.getElementById('zonavehiculos').value;
    let limitecarros = document.getElementById('limitecarros').value;
    let limitemotos = document.getElementById('limitemotos').value;
    let tarifas = document.getElementById('tarifas').value;
    let datos = {'idplazadisponible':idplazadisponible,'zonavehiculos':zonavehiculos,'limitecarros':limitecarros,'limitemotos':limitemotos,'tarifas':tarifas}
    let request = sendRequest('plazadisponible/', idplazadisponible ? 'PUT':'POST', datos)
    request.onload = function()
    {
        window.location = "plazadisponible.html";
    }
    request.onerror = function()
    {
        alert("Error al guardar los cambios.");
    }
}