//METODO PARA LISTAR
function listarVehiculos()
{
    let request = sendRequest('vehiculo/list', 'GET', '');
    let tabla = document.getElementById('tablavehiculo');
    tabla.innerHTML = "";
    request.onload = function()
    {
        let datos = request.response;
        datos.forEach(element =>
        {
            tabla.innerHTML += `
            <tr>
                <th scope="row">${element.idvehiculo}</th>                               
                <td>${element.tipovehiculo}</td>
                <td>${element.matricula}</td>
                <td>${element.descripcion}</td>               
                <td>
                    <button type="button" class="btn btn-primary" 
                    onclick='window.location = "form_vehiculo.html?id=${element.idvehiculo}"'>Editar</button>
                    <button type="button" class="btn btn-danger" 
                    onclick='borrarVehiculo(${element.idvehiculo})'>Eliminar</button>
                </td>
            </tr>
            `
        });
    }
    request.onerror = function()
    {
        tabla.innerHTML = `
        <tr>
            <td colspan='6'>Error al cargar los datos</td>
        </tr>
        `;
    }
}

//METODO PARA BORRAR
function borrarVehiculo(id)
{
    let request = sendRequest('vehiculo/'+id, 'DELETE', '');
    request.onload = function()
    {
        listarVehiculos();
    }
}

//METODO PARA CARGAR
function cargarVehiculo(id)
{
    let request = sendRequest('vehiculo/list/'+id, 'GET', '');
    let idvehiculo = document.getElementById('idvehiculo');
    let idcliente = document.getElementById('idcliente');
    let tipovehiculo = document.getElementById('tipovehiculo');
    let matricula = document.getElementById('matricula');
    let descripcion = document.getElementById('descripcion');
    request.onload = function()
    {
        let datos = request.response;
        idvehiculo.value = datos.idvehiculo;
        idcliente.value = datos.idcliente;
        tipovehiculo.value = datos.tipovehiculo;
        matricula.value = datos.matricula;
        descripcion.value = datos.descripcion;        
    }
    request.onerror = function()
    {
        alert("Error al cargar los datos");
    }
}

//METODO PARA AGREGAR O ACTUALIZAR
function grabarVehiculo()
{
    let idvehiculo = document.getElementById('idvehiculo').value;
    let idcliente = document.getElementById('idcliente').value;
    let tipovehiculo = document.getElementById('tipovehiculo').value;
    let matricula = document.getElementById('matricula').value;
    let descripcion = document.getElementById('descripcion').value;
    let datos = 
    {
        'idvehiculo':idvehiculo,
        'idcliente' : idcliente,
        'tipovehiculo':tipovehiculo,
        'matricula':matricula,
        'descripcion':descripcion
    }
    let request = sendRequest('vehiculo/', idcliente ? 'PUT':'POST', datos)
    request.onload = function()
    {
        window.location = "vehiculo.html";
    }
    request.onerror = function()
    {
        alert("Error al guardar.");
    }
}