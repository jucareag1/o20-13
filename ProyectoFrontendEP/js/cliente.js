function listarClientes()
{
    let request = sendRequest('cliente/list', 'GET', '');
    let tabla = document.getElementById('tablacliente');
    tabla.innerHTML = "";
    request.onload = function()
    {
        let datos = request.response;
        datos.forEach(element =>
        {
            tabla.innerHTML += `
            <tr>
                <th scope="row">${element.idcliente}</th>              
                <td>${element.nombre}</td>
                <td>${element.telefono}</td>
                <td>${element.correo}</td>                
                <td>
                    <button type="button" class="btn btn-primary" 
                    onclick='window.location = "form_cliente.html?id=${element.idcliente}"'>Editar</button>
                    <button type="button" class="btn btn-danger" 
                    onclick='borrarCliente(${element.idcliente})'>Eliminar</button>
                </td>
            </tr>
            `
        });
    }
    request.onerror = function()
    {
        tabla.innerHTML = `
        <tr>
            <td colspan='6'>Error al cargar los datos</td>
        </tr>
        `;
    }
}

//METODO PARA BORRAR
function borrarCliente(id)
{
    let request = sendRequest('cliente/'+id, 'DELETE', '');
    request.onload = function()
    {
        listarClientes();
    }
}

//METODO PARA CARGAR
function cargarCliente(id)
{
    let request = sendRequest('cliente/list/'+id, 'GET', '');
    let idcliente = document.getElementById('idcliente');
    let idplazadisponible = document.getElementById('idplazadisponible');
    let nombre = document.getElementById('nombre');
    let telefono = document.getElementById('telefono');
    let correo = document.getElementById('correo');
    request.onload = function()
    {
        let datos = request.response;
        idcliente.value = datos.idcliente;
        idplazadisponible.value = datos.idplazadisponible;
        nombre.value = datos.nombre;
        telefono.value = datos.telefono;
        correo.value = datos.correo;
    }
    request.onerror = function()
    {
        alert("Error al cargar los datos");
    }
}

//METODO PARA AGREGAR O ACTUALIZAR
function grabarCliente()
{
    let idcliente = document.getElementById('idcliente').value;
    let idplazadisponible = document.getElementById('idplazadisponible').value;
    let nombre = document.getElementById('nombre').value;
    let telefono = document.getElementById('telefono').value;
    let correo = document.getElementById('correo').value;
    let datos = 
    {
        'idcliente':idcliente,
        'idplazadisponible':idplazadisponible,
        'nombre':nombre,
        'telefono':telefono, 
        'correo':correo
    }
    let request = sendRequest('cliente/', idcliente ? 'PUT':'POST', datos)
    request.onload = function()
    {
        window.location = "cliente.html";
    }
    request.onerror = function()
    {
        alert("Error al guardar.");
    }
}