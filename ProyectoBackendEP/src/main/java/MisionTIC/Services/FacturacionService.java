package MisionTIC.Services;

import MisionTIC.Models.Facturacion;
import java.util.List;

public interface FacturacionService 
{
    public Facturacion save(Facturacion facturacion);
    public void delete(Integer id);
    public Facturacion findById(Integer id);
    public List<Facturacion> findAll();
}
