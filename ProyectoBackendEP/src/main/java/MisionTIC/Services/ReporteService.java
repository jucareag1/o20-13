package MisionTIC.Services;

import MisionTIC.Models.Reporte;
import java.util.List;

public interface ReporteService 
{
    public Reporte save(Reporte reporte);
    public void delete(Integer id);
    public Reporte findById(Integer id);
    public List<Reporte> findAll();
}
