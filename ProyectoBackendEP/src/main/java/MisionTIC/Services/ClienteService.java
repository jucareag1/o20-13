package MisionTIC.Services;

import MisionTIC.Models.Cliente;
import java.util.List;

public interface ClienteService 
{
    public Cliente save(Cliente cliente);
    public void delete(Integer id);
    public Cliente findById(Integer id);
    public List<Cliente> findAll();
}
