package MisionTIC.Services;

import MisionTIC.Models.Plazadisponible;
import java.util.List;

public interface PlazadisponibleService 
{
    public Plazadisponible save(Plazadisponible plazadisponible);
    public void delete(Integer id);
    public Plazadisponible findById(Integer id);
    public List<Plazadisponible> findAll();
}
