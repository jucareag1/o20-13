package MisionTIC.Services;

import MisionTIC.Models.Vehiculo;
import java.util.List;

public interface VehiculoService 
{
    public Vehiculo save(Vehiculo vehiculo);
    public void delete(Integer id);
    public Vehiculo findById(Integer id);
    public List<Vehiculo> findAll();
}
