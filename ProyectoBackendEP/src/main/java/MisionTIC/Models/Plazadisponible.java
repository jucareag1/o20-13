package MisionTIC.Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="plazadisponible")
public class Plazadisponible implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idplazadisponible")
    private Integer idplazadisponible;
    
    @Column(name="zonavehiculos")
    private String zonavehiculos;
    
    @Column(name="limitecarros")
    private String limitecarros;
    
    @Column(name="limitemotos")
    private String limitemotos;
    
    @Column(name="tarifas")
    private double tarifas;
    

    public Integer getIdplazadisponible() {
        return idplazadisponible;
    }

    public void setIdplazadisponible(Integer idplazadisponible) {
        this.idplazadisponible = idplazadisponible;
    }

    public String getZonavehiculos() {
        return zonavehiculos;
    }

    public void setZonavehiculos(String zonavehiculos) {
        this.zonavehiculos = zonavehiculos;
    }

    public String getLimitecarros() {
        return limitecarros;
    }

    public void setLimitecarros(String limitecarros) {
        this.limitecarros = limitecarros;
    }

    public String getLimitemotos() {
        return limitemotos;
    }

    public void setLimitemotos(String limitemotos) {
        this.limitemotos = limitemotos;
    }

    public double getTarifas() {
        return tarifas;
    }

    public void setTarifas(double tarifas) {
        this.tarifas = tarifas;
    }  
}
