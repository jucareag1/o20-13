package MisionTIC.Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="facturacion")
public class Facturacion implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idfacturacion")
    private Integer idfacturacion;
    
    @ManyToOne
    @JoinColumn(name="idvehiculo")
    private Vehiculo vehiculo;
    
    @ManyToOne
    @JoinColumn(name="idcliente")
    private Cliente cliente;
    
    @ManyToOne
    @JoinColumn(name="idplazadisponible")
    private Plazadisponible plazadisponible;
    
    @ManyToOne
    @JoinColumn(name="idusuario")
    private Usuario usuario;
    
    @Column(name="entrada")
    private String entrada;
    
    @Column(name="salida")
    private String salida;
    
    @Column(name="valorapagar")
    private double valorapagar;
    

    public Integer getIdfacturacion() {
        return idfacturacion;
    }

    public void setIdfacturacion(Integer idfacturacion) {
        this.idfacturacion = idfacturacion;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Plazadisponible getPlazadisponible() {
        return plazadisponible;
    }

    public void setPlazadisponible(Plazadisponible plazadisponible) {
        this.plazadisponible = plazadisponible;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getEntrada() {
        return entrada;
    }

    public void setEntrada(String entrada) {
        this.entrada = entrada;
    }

    public String getSalida() {
        return salida;
    }

    public void setSalida(String salida) {
        this.salida = salida;
    }

    public double getValorapagar() {
        return valorapagar;
    }

    public void setValorapagar(double valorapagar) {
        this.valorapagar = valorapagar;
    }
}
