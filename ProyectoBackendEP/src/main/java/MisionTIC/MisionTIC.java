package MisionTIC;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MisionTIC
{

	public static void main(String[] args)
        {
		SpringApplication.run(MisionTIC.class, args);
	}

}
