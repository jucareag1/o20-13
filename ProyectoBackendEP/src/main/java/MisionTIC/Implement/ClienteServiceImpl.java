package MisionTIC.Implement;

import MisionTIC.Dao.ClienteDao;
import MisionTIC.Models.Cliente;
import MisionTIC.Services.ClienteService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ClienteServiceImpl implements ClienteService
{
    @Autowired
    private ClienteDao clienteDao;

    //AGREGAR-EDITAR
    @Override
    @Transactional(readOnly=false)
    public Cliente save(Cliente cliente)
    {
        return clienteDao.save(cliente);
    }

    //BORRAR
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id)
    {
        clienteDao.deleteById(id);
    }

    //BUSCAR POR ID
    @Override
    @Transactional(readOnly=true)
    public Cliente findById(Integer id)
    {    
        return clienteDao.findById(id).orElse(null);
    }

    //BUSCAR TODOS
    @Override
    @Transactional(readOnly=true)
    public List<Cliente> findAll()
    {
        return (List<Cliente>) clienteDao.findAll();
    }
}
