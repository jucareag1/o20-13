package MisionTIC.Implement;

import MisionTIC.Dao.VehiculoDao;
import MisionTIC.Models.Vehiculo;
import MisionTIC.Services.VehiculoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class VehiculoServiceImpl implements VehiculoService
{
    @Autowired
    private VehiculoDao vehiculoDao;

    //AGREGAR-EDITAR
    @Override
    @Transactional(readOnly=false)
    public Vehiculo save(Vehiculo vehiculo)
    {
        return vehiculoDao.save(vehiculo);
    }

    //BORRAR
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id)
    {
        vehiculoDao.deleteById(id);
    }

    //BUSCAR POR ID
    @Override
    @Transactional(readOnly=true)
    public Vehiculo findById(Integer id)
    {    
        return vehiculoDao.findById(id).orElse(null);
    }

    //BUSCAR TODOS
    @Override
    @Transactional(readOnly=true)
    public List<Vehiculo> findAll()
    {
        return (List<Vehiculo>) vehiculoDao.findAll();
    }
}
