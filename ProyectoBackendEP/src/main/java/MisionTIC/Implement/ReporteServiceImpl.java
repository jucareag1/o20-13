package MisionTIC.Implement;

import MisionTIC.Dao.ReporteDao;
import MisionTIC.Models.Reporte;
import MisionTIC.Services.ReporteService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ReporteServiceImpl implements ReporteService
{
    @Autowired
    private ReporteDao reporteDao;

    //AGREGAR-EDITAR
    @Override
    @Transactional(readOnly=false)
    public Reporte save(Reporte reporte)
    {
        return reporteDao.save(reporte);
    }

    //BORRAR
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id)
    {
        reporteDao.deleteById(id);
    }

    //BUSCAR POR ID
    @Override
    @Transactional(readOnly=true)
    public Reporte findById(Integer id)
    {    
        return reporteDao.findById(id).orElse(null);
    }

    //BUSCAR TODOS
    @Override
    @Transactional(readOnly=true)
    public List<Reporte> findAll()
    {
        return (List<Reporte>) reporteDao.findAll();
    }
}
