package MisionTIC.Implement;

import MisionTIC.Dao.PlazadisponibleDao;
import MisionTIC.Models.Plazadisponible;
import MisionTIC.Services.PlazadisponibleService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PlazadisponibleServiceImpl implements PlazadisponibleService 
{
    @Autowired
    private PlazadisponibleDao plazadisponibleDao;

    //AGREGAR-EDITAR
    @Override
    @Transactional(readOnly=false)
    public Plazadisponible save(Plazadisponible plazadisponible)
    {
        return plazadisponibleDao.save(plazadisponible);
    }

    //BORRAR
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id)
    {
        plazadisponibleDao.deleteById(id);
    }

    //BUSCAR POR ID
    @Override
    @Transactional(readOnly=true)
    public Plazadisponible findById(Integer id)
    {    
        return plazadisponibleDao.findById(id).orElse(null);
    }

    //BUSCAR TODOS
    @Override
    @Transactional(readOnly=true)
    public List<Plazadisponible> findAll()
    {
        return (List<Plazadisponible>) plazadisponibleDao.findAll();
    }
}
