package MisionTIC.Implement;

import MisionTIC.Dao.FacturacionDao;
import MisionTIC.Models.Facturacion;
import MisionTIC.Services.FacturacionService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class FacturacionServiceImpl implements FacturacionService
{
    @Autowired
    private FacturacionDao facturacionDao;

    //AGREGAR-EDITAR
    @Override
    @Transactional(readOnly=false)
    public Facturacion save(Facturacion facturacion)
    {
        return facturacionDao.save(facturacion);
    }

    //BORRAR
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id)
    {
        facturacionDao.deleteById(id);
    }

    //BUSCAR POR ID
    @Override
    @Transactional(readOnly=true)
    public Facturacion findById(Integer id)
    {    
        return facturacionDao.findById(id).orElse(null);
    }

    //BUSCAR TODOS
    @Override
    @Transactional(readOnly=true)
    public List<Facturacion> findAll()
    {
        return (List<Facturacion>) facturacionDao.findAll();
    }
}
