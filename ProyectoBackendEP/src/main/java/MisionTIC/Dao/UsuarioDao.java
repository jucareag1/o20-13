package MisionTIC.Dao;
import MisionTIC.Models.Usuario;
import org.springframework.data.repository.CrudRepository;
public interface UsuarioDao extends CrudRepository<Usuario,Integer>{}