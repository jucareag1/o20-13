package MisionTIC.Dao;

import MisionTIC.Models.Vehiculo;
import org.springframework.data.repository.CrudRepository;

public interface VehiculoDao extends CrudRepository<Vehiculo,Integer>{
}
