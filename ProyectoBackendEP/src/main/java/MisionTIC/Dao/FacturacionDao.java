package MisionTIC.Dao;

import MisionTIC.Models.Facturacion;
import org.springframework.data.repository.CrudRepository;

public interface FacturacionDao extends CrudRepository<Facturacion,Integer>{
}
