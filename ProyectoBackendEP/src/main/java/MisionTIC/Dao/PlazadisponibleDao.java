package MisionTIC.Dao;

import MisionTIC.Models.Plazadisponible;
import org.springframework.data.repository.CrudRepository;

public interface PlazadisponibleDao extends CrudRepository<Plazadisponible,Integer>{   
}
