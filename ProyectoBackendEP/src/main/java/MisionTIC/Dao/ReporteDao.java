package MisionTIC.Dao;

import MisionTIC.Models.Reporte;
import org.springframework.data.repository.CrudRepository;

public interface ReporteDao extends CrudRepository<Reporte,Integer>{ 
}
