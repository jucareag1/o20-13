package MisionTIC.Dao;

import MisionTIC.Models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteDao extends CrudRepository<Cliente,Integer>{
}
