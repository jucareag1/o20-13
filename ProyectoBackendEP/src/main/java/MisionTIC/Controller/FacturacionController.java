package MisionTIC.Controller;

import MisionTIC.Models.Facturacion;
import MisionTIC.Services.FacturacionService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/facturacion")
public class FacturacionController 
{
    @Autowired
    private FacturacionService facturacionService;
    
    //AGREGAR
    @PostMapping(value="/")
    public ResponseEntity<Facturacion> agregar(@RequestBody Facturacion facturacion)
    {
        Facturacion obj = facturacionService.save(facturacion);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    //BORRAR
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Facturacion> eliminar(@PathVariable Integer id){
    Facturacion obj = facturacionService.findById(id);
    if(obj!=null)
        facturacionService.delete(id);
    else
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    //EDITAR
    @PutMapping(value="/")
    public ResponseEntity<Facturacion> editar(@RequestBody Facturacion facturacion)
    {
        Facturacion obj = facturacionService.findById(facturacion.getIdfacturacion());
        if(obj!=null)
        {
            obj.setVehiculo(facturacion.getVehiculo());
            obj.setCliente(facturacion.getCliente());
            obj.setPlazadisponible(facturacion.getPlazadisponible());
            obj.setUsuario(facturacion.getUsuario());
            obj.setEntrada(facturacion.getEntrada());
            obj.setSalida(facturacion.getSalida());
            obj.setValorapagar(facturacion.getValorapagar());
            facturacionService.save(obj);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    //BUSCAR TODOS
    @GetMapping("/list")
    public List<Facturacion> consultarTodo()
    {
        return facturacionService.findAll();
    }

    //BUSCAR POR ID
    @GetMapping("/list/{id}")
    public Facturacion consultaPorId(@PathVariable Integer id)
    {
        return facturacionService.findById(id);
    }
}
