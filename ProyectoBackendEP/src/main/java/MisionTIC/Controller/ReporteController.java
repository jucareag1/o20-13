package MisionTIC.Controller;

import MisionTIC.Models.Reporte;
import MisionTIC.Services.ReporteService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/reporte")
public class ReporteController 
{
    @Autowired
    private ReporteService reporteService;
    
    //AGREGAR
    @PostMapping(value="/")
    public ResponseEntity<Reporte> agregar(@RequestBody Reporte reporte)
    {
        Reporte obj = reporteService.save(reporte);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    //BORRAR
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Reporte> eliminar(@PathVariable Integer id){
    Reporte obj = reporteService.findById(id);
    if(obj!=null)
        reporteService.delete(id);
    else
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    //EDITAR
    @PutMapping(value="/")
    public ResponseEntity<Reporte> editar(@RequestBody Reporte reporte)
    {
        Reporte obj = reporteService.findById(reporte.getIdreporte());
        if(obj!=null)
        {
            obj.setCliente(reporte.getCliente());
            obj.setFacturacion(reporte.getFacturacion());
            obj.setTiempoconcepto(reporte.getTiempoconcepto()); 
            reporteService.save(obj);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    //BUSCAR TODOS
    @GetMapping("/list")
    public List<Reporte> consultarTodo()
    {
        return reporteService.findAll();
    }

    //BUSCAR POR ID
    @GetMapping("/list/{id}")
    public Reporte consultaPorId(@PathVariable Integer id)
    {
        return reporteService.findById(id);
    }
}
