package MisionTIC.Controller;

import MisionTIC.Models.Usuario;
import MisionTIC.Services.UsuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/usuarios")
public class UsuarioController
{

    @Autowired
    private UsuarioService usuarioservice;
    
    //AGREGAR
    @PostMapping(value="/")
    public ResponseEntity<Usuario> agregar(@RequestBody Usuario usuario)
    {
        Usuario obj = usuarioservice.save(usuario);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    //BORRAR
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Usuario> eliminar(@PathVariable Integer id){
    Usuario obj = usuarioservice.findById(id);
    if(obj!=null)
        usuarioservice.delete(id);
    else
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    //EDITAR
    @PutMapping(value="/")
    public ResponseEntity<Usuario> editar(@RequestBody Usuario usuario)
    {
        Usuario obj = usuarioservice.findById(usuario.getIdusuario());
        if(obj!=null)
        {
            obj.setUsuario(usuario.getUsuario());
            obj.setNombre(usuario.getNombre());
            obj.setCedula(usuario.getCedula());
            obj.setClave(usuario.getClave());
            obj.setTelefono(usuario.getTelefono());
            obj.setCorreo(usuario.getCorreo());
            obj.setActivo(usuario.getActivo());
            usuarioservice.save(obj);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    //BUSCAR TODOS
    @GetMapping("/list")
    public List<Usuario> consultarTodo()
    {
        return usuarioservice.findAll();
    }

    //BUSCAR POR ID
    @GetMapping("/list/{id}")
    public Usuario consultaPorId(@PathVariable Integer id)
    {
        return usuarioservice.findById(id);
    }
}
