package MisionTIC.Controller;

import MisionTIC.Models.Plazadisponible;
import MisionTIC.Services.PlazadisponibleService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/plazadisponible")
public class PlazadisponibleController 
{
    @Autowired
    private PlazadisponibleService plazadisponibleService;
    
    //AGREGAR
    @PostMapping(value="/")
    public ResponseEntity<Plazadisponible> agregar(@RequestBody Plazadisponible plazadisponible)
    {
        Plazadisponible obj = plazadisponibleService.save(plazadisponible);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    //BORRAR
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Plazadisponible> eliminar(@PathVariable Integer id){
    Plazadisponible obj = plazadisponibleService.findById(id);
    if(obj!=null)
        plazadisponibleService.delete(id);
    else
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    //EDITAR
    @PutMapping(value="/")
    public ResponseEntity<Plazadisponible> editar(@RequestBody Plazadisponible plazadisponible)
    {
        Plazadisponible obj = plazadisponibleService.findById(plazadisponible.getIdplazadisponible());
        if(obj!=null)
        {
            obj.setZonavehiculos(plazadisponible.getZonavehiculos());
            obj.setLimitecarros(plazadisponible.getLimitecarros());
            obj.setLimitemotos(plazadisponible.getLimitemotos());
            obj.setTarifas(plazadisponible.getTarifas());
            plazadisponibleService.save(obj);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    //BUSCAR TODOS
    @GetMapping("/list")
    public List<Plazadisponible> consultarTodo()
    {
        return plazadisponibleService.findAll();
    }

    //BUSCAR POR ID
    @GetMapping("/list/{id}")
    public Plazadisponible consultaPorId(@PathVariable Integer id)
    {
        return plazadisponibleService.findById(id);
    }
}
