package MisionTIC.Controller;

import MisionTIC.Models.Cliente;
import MisionTIC.Services.ClienteService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/cliente")
public class ClienteController 
{
    @Autowired
    private ClienteService clienteService;
    
    //AGREGAR
    @PostMapping(value="/")
    public ResponseEntity<Cliente> agregar(@RequestBody Cliente cliente)
    {
        Cliente obj = clienteService.save(cliente);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    //BORRAR
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Cliente> eliminar(@PathVariable Integer id){
    Cliente obj = clienteService.findById(id);
    if(obj!=null)
        clienteService.delete(id);
    else
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    //EDITAR
    @PutMapping(value="/")
    public ResponseEntity<Cliente> editar(@RequestBody Cliente cliente)
    {
        Cliente obj = clienteService.findById(cliente.getIdcliente());
        if(obj!=null)
        {
            obj.setPlazadisponible(cliente.getPlazadisponible());
            obj.setNombre(cliente.getNombre());
            obj.setTelefono(cliente.getTelefono());
            obj.setCorreo(cliente.getCorreo());
            clienteService.save(obj);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    //BUSCAR TODOS
    @GetMapping("/list")
    public List<Cliente> consultarTodo()
    {
        return clienteService.findAll();
    }

    //BUSCAR POR ID
    @GetMapping("/list/{id}")
    public Cliente consultaPorId(@PathVariable Integer id)
    {
        return clienteService.findById(id);
    }
}
