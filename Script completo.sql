CREATE TABLE IF NOT EXISTS usuarios(
idusuario INT NOT NULL AUTO_INCREMENT,
usuario VARCHAR(20) NOT NULL,
clave VARCHAR(10) NOT NULL,
nombre VARCHAR(45) NOT NULL,
cedula VARCHAR(20) NOT NULL,
telefono VARCHAR(30) NOT NULL,
correo VARCHAR(45) NOT NULL,
activo TINYINT(1) NOT NULL,
PRIMARY KEY (idusuario))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS plazadisponible(
idplazadisponible INT NOT NULL AUTO_INCREMENT,
zonavehiculos VARCHAR(10) NOT NULL,
limitecarros VARCHAR(10) NOT NULL,
limitemotos VARCHAR(10) NOT NULL,
tarifas DOUBLE NOT NULL,
PRIMARY KEY (idplazadisponible))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS cliente(
idcliente INT NOT NULL AUTO_INCREMENT,
idplazadisponible INT NOT NULL,
nombre VARCHAR(45) NOT NULL,
telefono VARCHAR(20) NOT NULL,
correo VARCHAR(45) NOT NULL,
PRIMARY KEY (idcliente),
FOREIGN KEY (idplazadisponible) REFERENCES plazadisponible (idplazadisponible))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS vehiculo(
idvehiculo INT NOT NULL AUTO_INCREMENT,
idcliente INT NOT NULL,
tipovehiculo VARCHAR(15) NOT NULL,
matricula VARCHAR(10) NOT NULL,
descripcion TEXT NOT NULL,
PRIMARY KEY (idvehiculo),
FOREIGN KEY (idcliente) REFERENCES cliente (idcliente))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS facturacion(
idfacturacion INT NOT NULL AUTO_INCREMENT,
idvehiculo INT NOT NULL,
idcliente INT NOT NULL,
idplazadisponible INT NOT NULL,
idusuario INT NOT NULL,
entrada DATETIME NOT NULL,
salida DATETIME NOT NULL,
valorapagar DOUBLE NOT NULL,
PRIMARY KEY (idfacturacion),
FOREIGN KEY (idusuario) REFERENCES usuarios (idusuario),
FOREIGN KEY (idvehiculo) REFERENCES vehiculo (idvehiculo),
FOREIGN KEY (idcliente) REFERENCES cliente (idcliente),
FOREIGN KEY (idplazadisponible) REFERENCES plazadisponible (idplazadisponible))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS reporte(
idreporte INT NOT NULL AUTO_INCREMENT,
idcliente INT NOT NULL,
idfacturacion INT NOT NULL,
tiempoconcepto VARCHAR(45) NOT NULL,
PRIMARY KEY (idreporte),
FOREIGN KEY (idfacturacion) REFERENCES facturacion (idfacturacion),
FOREIGN KEY (idcliente) REFERENCES cliente (idcliente))
ENGINE = InnoDB;


